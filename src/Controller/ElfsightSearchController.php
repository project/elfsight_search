<?php

namespace Drupal\elfsight_search\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightSearchController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/search/?utm_source=portals&utm_medium=drupal&utm_campaign=search&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
